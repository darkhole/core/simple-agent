Agent = require '@dark-hole/agent'

class SimpleAgent extends Agent

  constructor : ( uri, opts = {} ) ->

    throw new Error 'missing params' if not opts.action

    super uri, opts

    # recovery options
    @_action = opts.action

  setup : ->

    @socket.on 'exec', @_action

module.exports = SimpleAgent

